# recipe-app-api-proxy

NGINX proxy for our recipe app API

: '
    Usage

'

#===============Environment variables====================================
*  `LISTEN_PORT` -Port to listen on (default: `8000`). Port nginx server listens to connections on
* `APP_HOST` -Hostname of the app to forward requests to (default: `app`)
* `AP_PORT` -Port of the app to forward requests to (default: `9000`)

